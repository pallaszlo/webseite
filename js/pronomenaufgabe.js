// JavaScript Document
// eine Funktion für die Rückmeldung zu der Richtigkeit der gewählten Variante in der Aufgabe mit den Pronomen
function pronomen(){
	
	var form = document.getElementsByClassName("form");
	var antworten = document.getElementsByClassName("antwort");	
	var lösungen = document.getElementsByClassName("loesung");
	
	for(var i = 0;  i <form.length; i++){
		
		if(lösungen[i].checked === true){				
			antworten[i].style.color ="green";
			antworten[i].textContent ="richtig";			
						
		}else{			
			antworten[i].textContent ="falsch";			
			antworten[i].style.color ="red";				
		}
	}
	
}
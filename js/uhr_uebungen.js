/*global frage,appendRight,appendWrong,checkIfDone,clearRightWrong,zeitAusgabe*/
//Funktionen Globalisieren um Fehler zu vermeiden

//Parst die JSON-Datei und speichert sie in der Variable "JSON".
var JSON = JSON.parse(data);
var minuten = 0;
var zaehler = 0;
var richtig = 0;
var text = "Stell die Uhr bitte auf ";
var image = document.getElementById("uhrzeit");

//Setzte das Startbild der Uhr
var idiv = document.getElementById("quizprogress");
image.src = "../Bilder/Uhrzeiten/12_00.png";
document.getElementById("fragen").innerHTML = text + JSON[0].frage;

//Funktion um den Zähler i (wichtig für die Uhr) zu returnen. Wenn man this.i in der Funktion check aufruft, kommt es zu einer Verletzung des "use strict" - standards.
function getMinuten() {
    "use strict";
    return minuten;
}

//Funktion um die Eingabe des Benutzer mit der Lösung zu vergleichen 
function check() {
    "use strict";
    if (getMinuten() === JSON[zaehler].antwort) {
        document.getElementById("demo3").style.color = "green";
        document.getElementById("demo3").innerHTML = "Richtig!";
        richtig += 1;
        zaehler += 1;
        appendRight(zaehler);
        checkIfDone(zaehler);
        frage(zaehler);
    } else {
        document.getElementById("demo3").style.color = "red";
        document.getElementById("demo3").innerHTML = "Leider Falsch!";
        zaehler += 1;
        appendWrong(zaehler);
        checkIfDone(zaehler);
        frage(zaehler);
    }
}

//Funktion, um zu prüfen um das Quiz am ende ist
function checkIfDone(i) {
    "use strict";
    if (i === JSON.length) {
        document.getElementById("demo3").style.color = "white";
        document.getElementById("demo3").innerHTML = "Du hast " + richtig + " von " + JSON.length + " Fragen richtig!";
    }
}

//Funktion um die nächste Frage bereitzustellen
function frage(i) {
    "use strict";
    document.getElementById("fragen").innerHTML = text + JSON[i].frage;
}
    
//Funktion um die Uhr 5 Minuten zu Inkrementieren   
function clockForward() {
    "use strict";
    clearRightWrong();
	if (minuten === 55) {
        minuten = 0;
        document.getElementById("demo").innerHTML = "12:0" + minuten;
        zeitAusgabe(minuten);
    } else {
        if (minuten < 5) {
            minuten += 5;
            document.getElementById("demo").innerHTML = "12:0" + minuten;
            zeitAusgabe(minuten);
        } else {
            minuten += 5;
            document.getElementById("demo").innerHTML = "12:" + minuten;
            zeitAusgabe(minuten);
        }
    }
}

//Funktion um die Uhr 5 Minuten zu Dekrementieren
function clockBackward() {
    "use strict";
    clearRightWrong();
	if (minuten === 0) {
        minuten = 55;
        document.getElementById("demo").innerHTML = "12:" + minuten;
        zeitAusgabe(minuten);
    } else {
        if (minuten <= 10) {
            minuten -= 5;
            document.getElementById("demo").innerHTML = "12:0" + minuten;
            zeitAusgabe(minuten);
        } else {
            minuten -= 5;
            document.getElementById("demo").innerHTML = "12:" + minuten;
            zeitAusgabe(minuten);
        }
    }
    
}

//Funktion um die Anzeige für Richtig/Falsch zu clearen
function clearRightWrong() {
    "use strict";
    document.getElementById("demo3").innerHTML = " ";
}

//Funktion um den Rightblock in die Fortschrittsanzeige einzufügen
function appendRight(i) {
    "use strict";
    var idivr = document.createElement("div");
    idivr.className = "right";
    
    if (i === 1) {
        idivr.className += " firstelement";
        idiv.appendChild(idivr);
    } else if (i === JSON.length) {
        idivr.className += " lastelement";
        idiv.appendChild(idivr);
    } else {
        idiv.appendChild(idivr);
    }
}

//Funktion um den Wrongblock in die Fortschrittsanzeige einzufügen
function appendWrong(i) {
    "use strict";
    var idivw = document.createElement("div");
    idivw.className = "wrong";
    
    if (i === 1) {
        idivw.className += " firstelement";
        idiv.appendChild(idivw);
    } else if (i === JSON.length) {
        idivw.className += " lastelement";
        idiv.appendChild(idivw);
    } else {
        idiv.appendChild(idivw);
    }

}

//Funktion um die Uhr mit jeweiligem Bild auszugeben
function zeitAusgabe(i) {
	"use strict";
    switch (i) {
    case 0:
        image.src = "../Bilder/Uhrzeiten/12_00.png";
        return "Es ist ZWÖLF Uhr";
    case 5:
        image.src = "../Bilder/Uhrzeiten/12_05.png";
        return "Es ist FÜNF nach ZWÖLF";
    case 10:
        image.src = "../Bilder/Uhrzeiten/12_10.png";
        return "Es ist ZEHN nach ZWÖLF";
    case 15:
        image.src = "../Bilder/Uhrzeiten/12_15.png";
        return "Es ist VIERTEL nach ZWÖLF";
    case 20:
        image.src = "../Bilder/Uhrzeiten/12_20.png";
        return "Es ist ZWANZIG nach ZWÖLF";
    case 25:
        image.src = "../Bilder/Uhrzeiten/12_25.png";
        return "Es ist FÜNF vor HALB EINS";
    case 30:
        image.src = "../Bilder/Uhrzeiten/12_30.png";
        return "Es ist HALB EINS";
    case 35:
        image.src = "../Bilder/Uhrzeiten/12_35.png";
        return "Es ist FÜNF nach HALB";
    case 40:
        image.src = "../Bilder/Uhrzeiten/12_40.png";
        return "Es ist ZWANZIG vor EINS";
    case 45:
        image.src = "../Bilder/Uhrzeiten/12_45.png";
        return "Es ist VIERTEL vor EINS";
    case 50:
        image.src = "../Bilder/Uhrzeiten/12_50.png";
        return "Es ist ZEHN vor EINS";
    case 55:
        image.src = "../Bilder/Uhrzeiten/12_55.png";
        return "Es ist FÜNF vor EINS";
    case 60:
        image.src = "../Bilder/Uhrzeiten/12_00.png";
        return "Es ist ZWÖLF Uhr";
	}
}
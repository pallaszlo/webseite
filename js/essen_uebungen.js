// hole alle Eingabefelder
function declare() {
 eingabeFelder = document.getElementsByClassName('eingabeFeld');
}

function ueberpruefen() {

var richtigButton = document.getElementById('richtigButton');
var richtig = true;
// ändere Hintergrundfarben entsprechend richtiger und falscher Lösungen
  for (var i = 0; i < eingabeFelder.length; i++) {
     if (eingabeFelder[i].value == eingabeFelder[i].name) {
      eingabeFelder[i].style.background = "green";
     } else {
      eingabeFelder[i].style.background = "red";
      richtig = false;
     }
   }
   if (richtig) {
     richtigButton.style.background = "green";
   } else {
     richtigButton.style.background = "red";
   }
}

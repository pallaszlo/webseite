// JavaScript Document
//eine Funktion, die kontrolliert, ob die Eingabe bei der Übung zum Thema Negation richtig sind  
function aufgabeNegation(){
	"use strict";
	var antworten = document.getElementsByClassName("antwort");
	var lösungen =["ist nicht alt", "habe keine Katze", "muss nicht viel arbeiten", "ist nicht zu süß", "trinke keine Cola", "trinke nicht gern Cola","haben keine Kinder", "machst es nicht richtig", "komme nicht aus Frankreich", "gehe heute ins Kino nicht"];
	var eingabe,i;
	var x = document.getElementById("formsatz");
	eingabe = [];    	
    for (i = 0; i < x.length ;i++) {
        eingabe.push(x.elements[i].value);
    }	
	for(i = 0;  i <antworten.length; i++){
		
		if(eingabe[i] === lösungen[i]){
				antworten[i].style.border = "solid 1px";	
				antworten[i].style.color ="green";
				antworten[i].textContent ="richtig";
				antworten[i].style.padding = "0px 3px";	
				
		}
		else{
				antworten[i].style.border = "solid 1px";
				antworten[i].textContent ="falsch";			
				antworten[i].style.color ="red";
				antworten[i].style.padding = "0px 3px";	
					
		}
	}
}
/*global resetInput,gegenstand,ort*/
//Parst die JSON-Datei und speichert sie in der Variable "JSON".
var JSON = JSON.parse(data);

//Variable für das iterieren durch die JSON-Objekte.
var i = 0;

//Setzt das erste Bild für die Aufgabe.
var image = document.getElementById("uhrzeit");
image.src = JSON[i].bild;
  

function check() {
    "use strict";
    //Holt sich die beiden input-felder.
    var gegenstand = document.getElementById("gegenstand");
    var ort = document.getElementById("ort");
        
    /*Guckt, ob die Eingabe mit den in den JSON hinterlegten Lösungen übereinstimmen.
    Wenn beides Stimmt, wird der border beider Felder kurz grün und das Bild wechselt.
    Die Zählvariable i wird um 1 inkrementiert*/
    if (ort.value === JSON[i].ort && gegenstand.value === JSON[i].gegenstand) {
            
        gegenstand.style.borderColor = "green";
        ort.style.borderColor = "green";
            
        i += 1;
        image.src = JSON[i].bild;
            
        setInterval(resetInput, 1000);
        setInterval(resetForm, 1000);

        //Für den Fall, dass beides Eingaben falsch sind, werden die Border der Eingabefelder kurzweilig auf rot gesetzt.
    } else if (ort.value !== JSON[i].ort && gegenstand.value !== JSON[i].gegenstand) {
                
        gegenstand.style.borderColor = "red";
        ort.style.borderColor = "red";
                
        setInterval(resetInput, 1000);
            
        //Für den Fall, dass der Ort falsch ist, wird der Border des Eingabefeldes auf rot gesetzt.    
    } else if (ort.value !== JSON[i].ort) {
    
        ort.style.borderColor = "red";
        setInterval(resetInput, 1000);
            
        //Für den Fall, dass der Name des Gegenstandes falsch ist, wird der Border des Eingabefeldes auf rot gesetzt.
    } else if (gegenstand.value !== JSON[i].gegenstand) {
                
        gegenstand.style.borderColor = "red";
        setInterval(resetInput, 1000);
    }
}

    //Funktion um die Border der Eingabefelder zurückzusetzen.
function resetInput() {
    "use strict";
    gegenstand.style.borderColor = "white";
    ort.style.borderColor = "white"; 
}

function resetForm() {
    "use strict";
    gegenstand.value ="";
    ort.value ="";  
}
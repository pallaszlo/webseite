// JavaScript Document


/*die Funktion für die Kontrolle der Eingaben bei der Übung 1 bei dem Thema Zahlen*/
function aufgabeZahlen(){
	"use strict";
	var antworten = document.getElementsByClassName("antwort");
	var zahlen =["neunzehnhundertsechsunddreißig","siebzehn", "neununddreißig", "zweihundertfünfundvierzigtausendsiebenhundertachtundneunzig", "elftausenddreihundert", "neunundneunzig", "neunundsechzig", "neunzehnhundertfünfundsiebzig","zweitausendzwölf", "siebenunddreißig"];
	var eingabe,i;
	var x = document.getElementById("formsatz");
	eingabe = [];    	
    for (i = 0; i < x.length ;i++) {
        eingabe.push(x.elements[i].value);
    }	
	for(i = 0;  i <antworten.length; i++){
		
		if(eingabe[i] === zahlen[i]){
				antworten[i].style.border = "solid 1px";	
				antworten[i].style.color ="green";
				antworten[i].textContent ="richtig";
				antworten[i].style.padding = "0px 3px";	
		}
		else{
				antworten[i].style.border = "solid 1px";
				antworten[i].textContent ="falsch";			
				antworten[i].style.color ="red";
				antworten[i].style.padding = "0px 3px";			
		}
	}
}
/*die Funktion für die Kontrolle der Eingaben bei der Übung 2 bei dem Thema Zahlen*/
function aufgabeBuchstaben(){
	"use strict";
	var antworten = document.getElementsByClassName("antwortB");
	var zahlen =["1317", "842", "1134", "200700", "60309", "38000", "4047", "325", "21700", "1989"];
	var eingabe,i;
	var x = document.getElementById("formzahl");
	eingabe = [];    	
    for (i = 0; i < x.length ;i++) {
        eingabe.push(x.elements[i].value);
    }
		
	for(i = 0;  i <antworten.length; i++){
		if(eingabe[i] === zahlen[i]){
				antworten[i].style.border = "solid 1px";	
				antworten[i].style.color ="green";
				antworten[i].textContent ="richtig";
				antworten[i].style.padding = "0px 3px";	
		}
		else{
				antworten[i].style.border = "solid 1px";
				antworten[i].textContent ="falsch";			
				antworten[i].style.color ="red";
				antworten[i].style.padding = "0px 3px";			
		}
	}
}
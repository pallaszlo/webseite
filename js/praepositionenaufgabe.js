// JavaScript Document

//eine Funktion für die Kontrolle der Eingaben bei der Übung zu den Präpositionen. Als Rückmeldung bekommt der Benutzer eine Prozentzahl der richtigen Lösungen
function praepositionen(){
	"use strict";	
	var antworten = document.getElementsByClassName("antwort");	
	var lösungen = document.getElementsByClassName("loesung");
	var fL = document.getElementsByClassName("falscheloesung");
	var fantworten = document.getElementsByClassName("fantwort");	
	var i;
	var anzahl = document.getElementById("anzahl");
	var zähler = 0;
	
	for(i = 0;  i <fL.length; i++){			
		if(fL[i].checked === true){
			fantworten[i].textContent ="falsch";			
			fantworten[i].style.color ="red";			
			}		
	}		
	for(i = 0;  i <lösungen.length; i++){		
		
		if(lösungen[i].checked === true){						
			antworten[i].textContent ="richtig";			
			antworten[i].style.color ="green";
			zähler++;						
		}	
	}
	zähler = (zähler *100)/17;
	zähler = Math.floor(zähler);
	anzahl.textContent = "Dein Ergebnis: "+ zähler + " Prozent gut gemacht!";	
		
		
}
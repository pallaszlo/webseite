/*global zeitAusgabe*/
//Quellcode nach JSLint-Standard verbessert


//Deklaration wichtiger Variablen
var minuten = 0;
var text = "";

//Setzt das Startbild der Uhr
var image = document.getElementById("uhrzeit");
image.src = "../Bilder/Uhrzeiten/12_00.png";
    
//Funktion um die Uhr 5 Minuten zu Inkrementieren     
function clockForward() {
	"use strict";
    if (minuten === 55) {
        minuten = 0;
        document.getElementById("demo").innerHTML = "12:0" + minuten;
        document.getElementById("demo2").innerHTML = zeitAusgabe(minuten);
    } else {
        if (minuten < 5) {
            minuten += 5;
            document.getElementById("demo").innerHTML = "12:0" + minuten;
            document.getElementById("demo2").innerHTML = zeitAusgabe(minuten);
        } else {
            minuten += 5;
            document.getElementById("demo").innerHTML = "12:" + minuten;
            document.getElementById("demo2").innerHTML = zeitAusgabe(minuten);
        }
    }
    
}

//Funktion um die Uhr 5 Minuten zu Dekrementieren 
function clockBackward() {
	"use strict";
    if (minuten === 0) {
        minuten = 55;
        document.getElementById("demo").innerHTML = "12:" + minuten;
        document.getElementById("demo2").innerHTML = zeitAusgabe(minuten);
    } else {
        if (minuten <= 10) {
            minuten -= 5;
            document.getElementById("demo").innerHTML = "12:0" + minuten;
            document.getElementById("demo2").innerHTML = zeitAusgabe(minuten);
        } else {
            minuten -= 5;
            document.getElementById("demo").innerHTML = "12:" + minuten;
            document.getElementById("demo2").innerHTML = zeitAusgabe(minuten);
        }
    }
    
}

//Funktion um die Uhr mit jeweiligem Bild auszugeben
function zeitAusgabe(i) {
	"use strict";
    switch (i) {
    case 0:
        image.src = "../Bilder/Uhrzeiten/12_00.png";
        return "Es ist <u>Punkt ZWÖLF Uhr</u>";
    case 5:
        image.src = "../Bilder/Uhrzeiten/12_05.png";
        return "Es ist <u>FÜNF nach ZWÖLF</u>";
    case 10:
        image.src = "../Bilder/Uhrzeiten/12_10.png";
        return "Es ist <u>ZEHN nach ZWÖLF</u>";
    case 15:
        image.src = "../Bilder/Uhrzeiten/12_15.png";
        return "Es ist <u>VIERTEL nach ZWÖLF</u>";
    case 20:
        image.src = "../Bilder/Uhrzeiten/12_20.png";
        return "Es ist <u>ZWANZIG nach ZWÖLF</u>";
    case 25:
        image.src = "../Bilder/Uhrzeiten/12_25.png";
        return "Es ist <u>FÜNF vor HALB EINS</u>";
    case 30:
        image.src = "../Bilder/Uhrzeiten/12_30.png";
        return "Es ist <u>HALB EINS</u>";
    case 35:
        image.src = "../Bilder/Uhrzeiten/12_35.png";
        return "Es ist <u>FÜNF nach HALB EINS</u>";
    case 40:
        image.src = "../Bilder/Uhrzeiten/12_40.png";
        return "Es ist <u>ZWANZIG vor EINS</u>";
    case 45:
        image.src = "../Bilder/Uhrzeiten/12_45.png";
        return "Es ist <u>VIERTEL vor EINS</u>";
    case 50:
        image.src = "../Bilder/Uhrzeiten/12_50.png";
        return "Es ist <u>ZEHN vor EINS</u>";
    case 55:
        image.src = "../Bilder/Uhrzeiten/12_55.png";
        return "Es ist <u>FÜNF vor EINS</u>";
    case 60:
        image.src = "../Bilder/Uhrzeiten/12_00.png";
        return "Es ist <u>Punkt ZWÖLF Uhr</u>";
	}
}
// JavaScript Document
"use strict";
// eine Funktion für das Schließens/Öffnen des Tabellencontainers der Ordinalzahlen
function öffnenOZ(){
	var tabellekZ = document.getElementById("tabelleKZ");
	var tabelleoZ = document.getElementById("tabelleoZ");
	
	tabellekZ.style.display = "none";
	tabelleoZ.style.display = "block";
}
// eine Funktion für das Schließens/Öffnen des Tabellencontainers der Kardinalzahlen
function öffnenKZ(){
	var tabellekZ = document.getElementById("tabelleKZ");
	var tabelleoZ = document.getElementById("tabelleoZ");
	
	tabellekZ.style.display = "block";
	tabelleoZ.style.display = "none";
}


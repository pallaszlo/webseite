'use strict';
// Öffnen des Login/Konto-Fenster
function showLogin() {
    var modal = document.getElementById('anmelden');
    modal.style.display = "block";
}
function showKonto() {
  var konto = document.getElementById('kontoerstellen');
	konto.style.display = "block";
}
// Schließen des Login/Konto-Fenster
function closeLogin() {
    var modal = document.getElementById('anmelden');
    modal.style.display = "none";
}
function closeKonto() {
  var konto = document.getElementById('kontoerstellen');
	konto.style.display = "none";
}

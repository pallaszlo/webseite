// hole alle inputFelder
felder = document.getElementsByClassName('inputFeld');

function show(feldName) {
  // Blende vorher alle Felder aus
  for(var i = 0; i < felder.length; i++) {

    felder[i].style.visibility = "hidden";
  }
  // blende das übergebene Feld ein
  var feld = document.getElementById(feldName);
  feld.style.visibility = "visible";
}
function ueberpruefeHobbies() {
  var richtig = true;
  for (var i = 0; i < felder.length; i++) {
    // hole den Namen des Feldes
    var nameVonFeld = felder[i].name;
    // entferne Leerzeichen aus dem Namen
    var nameID = nameVonFeld.replace(/\s/g, "");
    // Benutze Namen um auf die SVG-Elemente zuzugreifen
    var hintergrund = document.getElementById(nameID);

    if (felder[i].value == nameVonFeld) {
      // bei richtiger Eingabe, ändere Hintergrundfarbe auf Grün
      felder[i].style.background = "green";
      hintergrund.style.fill = "green";
     } else {
       // bei falscher Eingabe, ändere Hintergrundfarbe auf Rot
      felder[i].style.background = "red";
      hintergrund.style.fill = "red";
      richtig = false;
     }
   }
   // ändere entsprechend die Farbe des Richtig-Buttons
   if (richtig) {
     richtigButton.style.background = "green";
   } else {
     richtigButton.style.background = "red";
   }
}

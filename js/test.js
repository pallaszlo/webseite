
function testen(){
	"use strict";
	var antworten = document.getElementsByClassName("antwort");
	var lösungen = document.getElementsByClassName("lösung");
	var leer = document.getElementsByClassName("leer");
	var anzahl = document.getElementById("anzahl");
	var zähler = 0;
	for(var i = 0;  i <antworten.length; i++){
		if(leer[i].selected === true){
			antworten[i].textContent ="Wähle eine Variante aus!";

		}else if(lösungen[i].selected === true){
			antworten[i].style.border = "solid 1px";
			antworten[i].style.color ="green";
			antworten[i].textContent ="richtig";
			document.getElementsByClassName("select")[i].style.backgroundColor ="gold";
			zähler++;

		}else{
			antworten[i].style.border = "solid 1px";
			antworten[i].textContent ="falsch";
			antworten[i].style.color ="red";
			document.getElementsByClassName("select")[i].style.backgroundColor ="white";
		}
	}
	zähler = (zähler *100)/antworten.length;
	zähler = Math.floor(zähler);
	anzahl.textContent = "Dein Ergebnis: "+ zähler + " Prozent gut gemacht!";

}

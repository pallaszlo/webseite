// JavaScript Document
'use strict';
// eine Funktion für ein erlaubtes Ablegen der divs in Container
function ablegenErlauben(ev) {
	ev.preventDefault();
}
// eine Funktion, die das Ziehen der divs aus einem Container in einen anderen ermöglicht
function ziehen(ev) {
	ev.dataTransfer.setData('text', ev.target.id);
}
// eine Funktion, die das Hinzufügen der Kinderelemente ermöglicht
function ablegen(ev) {
	ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}
// eine Funktion für das Schließens des Modal-Fensters mit der positiven Bestätigung
function closeRückmeldung() {
    var modal = document.getElementById('rückmeldung');
    modal.style.display = "none";
}
// eine Funktion für das Schließens des Modal-Fensters mit der negativen Rückmeldung
function closeWarnung() {
    var modal = document.getElementById('warnung');
	var modalr = document.getElementById('rückmeldung');
    modal.style.display = "none";
	modalr.style.display = "none";
}
// eine Funktion für die Rückmeldung, ob die gewählten Elemente in die entsprechenden Kästchen eingeordnet wurden - Übung eins
function draganddrop(){	
	var i;
	var ichliste = document.getElementById("ich").querySelectorAll("p");    
   	for ( i = 0; i < ichliste.length; i++) {
			if (ichliste[i].id < 11){
				document.getElementById("rückmeldung").style.display ="block";					
			}
			else{
				document.getElementById("warnung").style.display = "block";					
				}				
    	}
	var duliste = document.getElementById("du").querySelectorAll("p");    
	for ( i = 0; i < duliste.length; i++) {		
        	if (((duliste[i].id > 10)&&(duliste[i].id < 21))||((duliste[i].id > 50)&&(duliste[i].id < 60))) {			
				document.getElementById("rückmeldung").style.display ="block";
			}
			else{
				document.getElementById("warnung").style.display = "block";
			}		
    	}		
		
	var erliste = document.getElementById("er").querySelectorAll("p");    
    	
		for ( i = 0; i < erliste.length; i++) {		
        	if (((erliste[i].id > 20)&&(erliste[i].id < 31))||((erliste[i].id > 51)&&(erliste[i].id < 60))||((erliste[i].id > 60)&&(erliste[i].id < 70))) {			
				document.getElementById("rückmeldung").style.display ="block";
			}
			else{
				document.getElementById("warnung").style.display = "block";
			}		
    	}
	var wirliste = document.getElementById("wir").querySelectorAll("p");    
    	
		for ( i = 0; i < wirliste.length; i++) {		
        	if ((wirliste[i].id > 30 )&&(wirliste[i].id < 41)) {			
				document.getElementById("rückmeldung").style.display ="block";
			}
			else{
				document.getElementById("warnung").style.display = "block";
			}		
    	}
	var ihrliste = document.getElementById("ihr").querySelectorAll("p");    
    	
		for ( i = 0; i < ihrliste.length; i++) {		
        	if (((ihrliste[i].id >40)&&(ihrliste[i].id < 51))||((ihrliste[i].id > 60)&&(ihrliste[i].id < 70))) {			
				document.getElementById("rückmeldung").style.display ="block";
			}
			else{
				document.getElementById("warnung").style.display = "block";
			}		
    	}
	var sieliste = document.getElementById("sie").querySelectorAll("p");    
    	
		for ( i = 0; i < sieliste.length; i++) {		
        	if ((sieliste[i].id > 30)&&(sieliste[i].id < 41)) {			
				document.getElementById("rückmeldung").style.display ="block";
			}
			else{
				document.getElementById("warnung").style.display = "block";
			}		
    	}
}
// eine Funktion für die Rückmeldung zu der Richtigkeit der gewählten Endungen in der Übung 2
function endungen(){
	var antworten = document.getElementsByClassName("antwort");	
	var lösungen = document.getElementsByClassName("lösung");
	var leer = document.getElementsByClassName("leer");
	for(var i = 0;  i <antworten.length; i++){
		if(leer[i].selected === true){			
			antworten[i].textContent ="Wähle eine Variante aus!";		
						
		}else if(lösungen[i].selected === true){
			antworten[i].style.border = "solid 1px";	
			antworten[i].style.color ="green";
			antworten[i].textContent ="richtig";
			document.getElementsByClassName("select")[i].style.backgroundColor ="gold";	
						
		}else{
			antworten[i].style.border = "solid 1px";
			antworten[i].textContent ="falsch";			
			antworten[i].style.color ="red";	
			document.getElementsByClassName("select")[i].style.backgroundColor ="white";	
		}
	}
	
}
// eine Funktion für das Vergleichen der Lösungsmenge mit der Eingabemenge bei der Übung 3
function verbformen(){
	var verben =["sieht","läuft", "wäscht", "fährt", "hilft", "Helft","isst", "wäschst", "gibt", "schläft", "gebt", "spricht","wisst","Schlaft", "nimmt"];
	var eingabe,i;
	var x = document.getElementById("frm1");
	eingabe = [];
    	
    for (i = 0; i < x.length ;i++) {
        eingabe.push(x.elements[i].value);
    }
	var vf = verben.toString();	
	var txt = eingabe.toString();		
	
	if(txt === vf){
		document.getElementById("rückmeldung").style.display ="block";		
	}
	else{
		document.getElementById("warnung").style.display ="block";		
		}
	
/*
l = verbformen.length;
text = "<ul>";
for (i = 1; i < l; i++) {
    text += "<li>" +i +". "+ verbformen[i] + "</li>";
}
text += "</ul>";
document.getElementById("formcontainer").innerHTML = text;
*/
}
// eine Funktion für das Zeigen der richtigen Lösungen als Hilfe für die Übung 3
function hilfe(){
	var verbformen =[" sieht"," läuft", " wäscht", " fährt", " hilft", " Helft"," isst", " wäschst", " gibt", " schläft", " gebt", " spricht"," wisst"," Schlaft", " nimmt"];
	document.getElementById("verbliste").innerHTML =verbformen;
	document.getElementById("verbliste").style.display="block";
}
// eine Funktion für das Schließens des hilfe-Fensters
function hilfeclose(){
	document.getElementById("verbliste").style.display="none";
}
// Funktionen für das Bezeichnen der aktuellen Kontroll-Buttons
function farbeEA(){
	var kontrolle = document.getElementById("kontrolle");	
	kontrolle.style.color = "white";
	kontrolle.style.backgroundColor = "#60203e";
	var kontrolleEnd = document.getElementById("kontrolleEnd");
	kontrolleEnd.style.color = "black";
	kontrolleEnd.style.backgroundColor = "gold";
	var kontrolleVerb = document.getElementById("kontrolleVerb");
	kontrolleVerb.style.color = "black";
	kontrolleVerb.style.backgroundColor = "gold";
}
function farbeZA(){
	var kontrolle = document.getElementById("kontrolle");	
	kontrolle.style.color = "black";
	kontrolle.style.backgroundColor = "gold";
	var kontrolleEnd = document.getElementById("kontrolleEnd");
	kontrolleEnd.style.color = "white";
	kontrolleEnd.style.backgroundColor = "#60203e";
	var kontrolleVerb = document.getElementById("kontrolleVerb");
	kontrolleVerb.style.color = "black";
	kontrolleVerb.style.backgroundColor = "gold";
}
function farbeDA(){
	var kontrolle = document.getElementById("kontrolle");	
	kontrolle.style.color = "black";
	kontrolle.style.backgroundColor = "gold";
	var kontrolleEnd = document.getElementById("kontrolleEnd");
	kontrolleEnd.style.color = "black";
	kontrolleEnd.style.backgroundColor = "gold";
	var kontrolleVerb = document.getElementById("kontrolleVerb");
	kontrolleVerb.style.color = "white";
	kontrolleVerb.style.backgroundColor = "#60203e";
}
// eine Funktion für das Öffnen der ersten Aufgabe und gleichzeitig Schließen der Übungen 2 und 3
function ersteAufgabe(){
	farbeEA();
	var i;
	var eA = document.querySelectorAll("#aufgabe, #verbcontent, #contentcont");
	var zA = document.querySelectorAll("#aufgabeZwei, #satzcontainer");
	var dA = document.querySelectorAll("#aufgabeDrei,#formcontainer");
			
	for(i = 0; i < eA.length; i++){
		eA[i].style.display = "flex";
		zA[i].style.display = "none";
		dA[i].style.display = "none";	
	}
		
		
}
// eine Funktion für das Öffnen der zweiten Aufgabe und gleichzeitig Schließen der Übungen 1 und 3
function zweiteAufgabe(){
	farbeZA();
	var i;
	var eA = document.querySelectorAll("#aufgabe, #verbcontent, #contentcont");	
	var zA = document.querySelectorAll("#aufgabeZwei, #satzcontainer");	
	var dA = document.querySelectorAll("#aufgabeDrei,#formcontainer");	
	
	for(i = 0; i < eA.length; i++){
		eA[i].style.display = "none";
		zA[i].style.display = "flex";
		dA[i].style.display = "none";
	}
	
}
// eine Funktion für das Öffnen der dritten Aufgabe und gleichzeitig Schließen der Übungen 1 und 2
function dritteAufgabe(){
	farbeDA();
	var i;
	var eA = document.querySelectorAll("#aufgabe, #verbcontent, #contentcont");	
	var zA = document.querySelectorAll("#aufgabeZwei, #satzcontainer");
	var dA = document.querySelectorAll(" #aufgabeDrei,#formcontainer ");		
	for(i = 0; i < eA.length; i++){
		eA[i].style.display = "none";
		zA[i].style.display = "none";
		dA[i].style.display = "flex";
		
	}
}